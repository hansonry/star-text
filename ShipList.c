#include "ShipList.h"
#include "Refmem.h"
#include "PlayerList.h"


REFMEM_CREATE_TYPE(ship, struct ship)

#define MAX_SHIP_COUNT 512

struct shipList
{
   struct ship shipMemData[MAX_SHIP_COUNT];
   struct refmementry shipMemEntries[MAX_SHIP_COUNT];
   struct refmem_ship shipMem;

   struct list_ship shipList;
   struct ship * shipListData[MAX_SHIP_COUNT];
};

static struct shipList ShipList;


void ShipList_Init(void)
{
   List_Init_ship(&ShipList.shipList, MAX_SHIP_COUNT, ShipList.shipListData);
   Refmem_Init_ship(&ShipList.shipMem, MAX_SHIP_COUNT, ShipList.shipMemData, ShipList.shipMemEntries);
}

struct ship ** ShipList_Get(size_t * count)
{
   return List_Get_ship(&ShipList.shipList, count);
}

bool Ship_Release(struct ship * ship)
{
   return Refmem_Release_ship(&ShipList.shipMem, ship);
}

void Ship_Take(struct ship * ship)
{
   Refmem_Take_ship(&ShipList.shipMem, ship);
}

void ShipList_RemoveDead(void)
{
   size_t i, count;
   struct ship ** ships = List_Get_ship(&ShipList.shipList, &count);
   for(i = count - 1; i < count; i--)
   {
      struct ship * ship = ships[i];
      if(ship->health == 0)
      {
         List_RemoveFast_ship(&ShipList.shipList, i);
         Refmem_Release_ship(&ShipList.shipMem, ship);
      }
   }
}

struct ship * ShipList_AddShip(struct player * owner, int x, int y, int z)
{
   struct ship * ship = Refmem_Alloc_ship(&ShipList.shipMem);
   List_Add_ship(&ShipList.shipList, ship, NULL);
   Position_Set(&ship->position,    x, y, z);
   Position_Set(&ship->movement.destination, x, y, z);
   ship->owner = owner;
   ship->health = 100;
   ship->weapons.target = NULL;
   ship->weapons.fireAtWill = false;
   ship->weapons.reloadTimer = 0;

   PlayerList_Take(owner);
   
   return ship;
}

void Ship_Kill(struct ship * ship)
{
   ship->health = 0;
}


