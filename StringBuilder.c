#include <stdlib.h>
#include <string.h>
#include "StringBuilder.h"

#define DEFAULT_GROWBY 32


void StringBuilder_Init(struct stringBuilder * builder, size_t growBy)
{

   if(growBy == 0)
   {
      builder->growBy = DEFAULT_GROWBY;
   }
   else
   {
      builder->growBy = growBy;
   }

   builder->base = NULL;
   builder->count = 0;
   builder->size = 0;
   
}

void StringBuilder_Free(struct stringBuilder * builder)
{
   if(builder->base != NULL)
   {
      free(builder->base);
      builder->base = NULL;
   }
   builder->count = 0;
   builder->size = 0;
   builder->growBy = 0;
}


static inline
void StringBuilder_MakeRoomFor(struct stringBuilder * builder, 
                               size_t size)
{
   if(builder->size < size)
   {
      size_t newSize = size + builder->growBy;
      builder->base = realloc(builder->base, newSize);
      builder->size = newSize;
   }
}

void StringBuilder_Add(struct stringBuilder * builder, const char * chars, 
                       size_t size)
{
   char * start;
   StringBuilder_MakeRoomFor(builder, builder->count + size);
   start = &builder->base[builder->count];
   memcpy(start, chars, size);
   builder->count += size;
}


void StringBuilder_Clear(struct stringBuilder * builder)
{
   builder->count = 0;
}


char * StringBuilder_Duplicate(const struct stringBuilder * builder)
{
   char * out = malloc(builder->count + 1);
   memcpy(out, builder->base, builder->count);
   out[builder->count] = '\0';
   return out;
}


