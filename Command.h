#ifndef __COMMAND_H__
#define __COMMAND_H__
#include <stdbool.h>
#include "Position.h"

struct ship;

enum commandType
{
   eCT_None,
   eCT_Move,
   eCT_Target,
   eCT_Weapons,
};


struct commandMove
{
   enum commandType type;
   struct position  destination;
};

struct commandTarget
{
   enum commandType type;
   struct ship * target;
};

struct commandWeapons
{
   enum commandType type;
   bool fireAtWill;
};

union command
{
   enum commandType      type;
   struct commandMove    move;
   struct commandTarget  target;
   struct commandWeapons weapons;
};


#endif // __COMMAND_H__
