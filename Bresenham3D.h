#ifndef __BRESENHAM3D_H__
#define __BRESENHAM3D_H__
#include <stdbool.h>
#include "Position.h"

struct bresenham3d
{
   enum positionComponent mix[3];
   struct position mixedDiffs;
   struct position mixedEpsilon;
   struct position mixedPosition;
   struct position mixedSign;
   int end;
};

void Bresenham3D_Init(struct bresenham3d * b3d,
                      const struct position * start, 
                      const struct position * end);

bool Bresenham3D_Next(struct bresenham3d * b3d,
                      struct position * next);

#endif // __BRESENHAM3D_H__

