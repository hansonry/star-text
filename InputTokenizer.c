#include <stdlib.h>
#include <string.h>
#include "InputTokenizer.h"

static inline
char GetChar(const struct inputTokenizerState * s)
{
   return Queue_Peek_char(s->input);
}

static inline
bool HasMoreChars(const struct inputTokenizerState * s)
{
   return Queue_GetUsedCount_char(s->input) > 0;
}

static inline
bool IsCharWhiteSpace(char c)
{
   return c == ' '  ||
          c == '\t' ||
          c == '\r' ||
          c == '\n';
}

static inline
bool IsCharDigit(char c)
{
   return c >= '0' && c <= '9';
}

static inline
bool IsCharInt(char c)
{
   return IsCharDigit(c) || c == '-';
}

static inline 
bool IsCharQuote(char c)
{
   return c == '"';
}

static inline
bool IsCharSlash(char c)
{
   return c == '/';
}

static inline
bool IsCharEndOfCommand(char c)
{
   return c == '\n';
}

static inline
bool IsInt(const struct inputTokenizerState * s)
{
   return IsCharInt(GetChar(s));
}

static inline
bool IsDigit(const struct inputTokenizerState * s)
{
   return IsCharDigit(GetChar(s));
}

static inline
bool IsWhiteSpace(const struct inputTokenizerState * s)
{
   return IsCharWhiteSpace(GetChar(s));
}

static inline
bool IsEndOfCommand(const struct inputTokenizerState * s)
{
   return IsCharEndOfCommand(GetChar(s));
}

static inline
bool IsSlash(const struct inputTokenizerState * s)
{
   return IsCharSlash(GetChar(s));
}

static inline
bool IsQuote(const struct inputTokenizerState * s)
{
   return IsCharQuote(GetChar(s));
}

static inline
char NextChar(struct inputTokenizerState * s)
{
   if(HasMoreChars(s))
   {
      char c = GetChar(s);
      (void)Queue_Dequeue_char(s->input);
      return c;
   }
   return '\0';
}

static
void StateUnknown(struct inputTokenizerState * s);

static inline
void EndToken(struct inputTokenizerState * s)
{
   s->token.text = StringBuilder_Duplicate(&s->stringBuilder);
   Queue_EnqueueCopy_inputToken(s->tokenList, &s->token, 1);
}

static inline
void EndWord(struct inputTokenizerState * s)
{
   s->state = StateUnknown;
   EndToken(s);
}

static inline
void EndInteger(struct inputTokenizerState * s)
{
   s->state = StateUnknown;
   s->token.text = StringBuilder_Duplicate(&s->stringBuilder);
   s->token.intValue = strtol(s->token.text, NULL, 0);
   Queue_EnqueueCopy_inputToken(s->tokenList, &s->token, 1);
}

static inline
void EndQuote(struct inputTokenizerState * s)
{
   s->state = StateUnknown;
   EndToken(s);
   (void)NextChar(s);
}

static
void StateWord(struct inputTokenizerState * s)
{
   StringBuilder_AddOne(&s->stringBuilder, NextChar(s));
   if(IsWhiteSpace(s) || IsQuote(s))
   {
      EndWord(s);   
   }
}

static
void StateInteger(struct inputTokenizerState * s)
{
   StringBuilder_AddOne(&s->stringBuilder, NextChar(s));
   if(!IsDigit(s) || IsQuote(s))
   {
      EndInteger(s);   
   }
}

static
void StateString(struct inputTokenizerState * s)
{
   char c = NextChar(s);
   if(c == '\\')
   {
      char cmd = NextChar(s);
      switch(cmd)
      {
         case '"':
            StringBuilder_AddOne(&s->stringBuilder, '"');
            break;
         case 't':
            StringBuilder_AddOne(&s->stringBuilder, '\t');
            break;
         case 'r':
            StringBuilder_AddOne(&s->stringBuilder, '\r');
            break;
         case 'n':
            StringBuilder_AddOne(&s->stringBuilder, '\n');
            break;
         case '\\':
            StringBuilder_AddOne(&s->stringBuilder, '\\');
            break;
         default:
            StringBuilder_AddOne(&s->stringBuilder, '?');
            break;
      }
   }
   else
   {
      StringBuilder_AddOne(&s->stringBuilder, c);
   }
   if(IsQuote(s))
   {
      EndQuote(s);   
   }
}

static inline
void StartToken(struct inputTokenizerState * s, enum inputTokenType type)
{
   s->token.type = type;
   StringBuilder_Clear(&s->stringBuilder);
}

static inline
void StartWord(struct inputTokenizerState * s)
{
   s->state = StateWord;
   s->token.intValue = 0;
   StartToken(s, eITT_Text);
}

static inline
void StartInteger(struct inputTokenizerState * s)
{
   s->state = StateInteger;
   StartToken(s, eITT_Integer);
}

static inline
void StartString(struct inputTokenizerState * s)
{
   s->state = StateString;
   s->token.intValue = 0;
   (void)NextChar(s);
   StartToken(s, eITT_String);
}

static
void StateIgnoreWhiteSpace(struct inputTokenizerState * s)
{
   if(IsWhiteSpace(s))
   {
      (void)NextChar(s);
   }
   else
   {
      s->state = StateUnknown;
   }
}

static inline
void ProcessSlash(struct inputTokenizerState * s)
{
   StartToken(s, eITT_Slash);
   s->token.intValue = 0;
   StringBuilder_AddOne(&s->stringBuilder, '/');
   EndToken(s);
   (void)NextChar(s);
}

static inline
void ProcessEndOfCommand(struct inputTokenizerState * s)
{
   StartToken(s, eITT_EndOfCommand);
   s->token.intValue = 0;
   StringBuilder_AddOne(&s->stringBuilder, '\n');
   EndToken(s);
   (void)NextChar(s);
}

static
void StateUnknown(struct inputTokenizerState * s)
{
   if(IsEndOfCommand(s))
   {
      ProcessEndOfCommand(s);
   }
   else if(IsWhiteSpace(s))
   {
      s->state = StateIgnoreWhiteSpace;
   }
   else if(IsSlash(s))
   {
      ProcessSlash(s);
   }
   else if(IsQuote(s))
   {
      StartString(s);
   }
   else if(IsInt(s))
   {
      StartInteger(s);
   }
   else
   {
      StartWord(s);
   }
}

void InputTokenizer_Init(struct inputTokenizerState * state, 
                         struct queue_inputToken * tokenList, 
                         struct queue_char * input)
{
   state->input = input;
   state->tokenList = tokenList;
   state->state = StateUnknown;
   StringBuilder_Init(&state->stringBuilder, 0);

}

void InputTokenizer_Free(struct inputTokenizerState * state)
{
   StringBuilder_Free(&state->stringBuilder);
   while(Queue_GetUsedCount_inputToken(state->tokenList) > 0)
   {
      struct inputToken *token = Queue_PeekPtr_inputToken(state->tokenList);
      InputTokenizer_FreeToken(token);
      Queue_Dequeue_inputToken(state->tokenList);
   }
}


void InputTokenizer_Tokenize(struct inputTokenizerState * state)
{

   while(HasMoreChars(state))
   {
      state->state(state);
   }
}

void InputTokenizer_FreeToken(struct inputToken * token)
{
   if(token->text != NULL)
   {
      free(token->text);
      token->text = NULL;
   }
}

