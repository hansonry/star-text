#ifndef __SIMULATION_H__
#define __SIMULATION_H__
#include "ShipList.h"
#include "PlayerList.h"
#include "Command.h"
#include "Event.h"

void Simulation_Init(void);

void Simulation_CommandShip(struct ship * ship, const union command * command); 

void Simulation_Step(void);

#endif // __SIMULATION_H__
