#ifndef __LIST_H__
#define __LIST_H__
#include <stddef.h>
#include <stdbool.h>

struct list
{
   void * base;
   size_t elementSize;
   size_t size;
   size_t count;
};

void List_Init(struct list * list, 
               size_t elementSize, 
               size_t numberOfElements, 
               void * data);
               
void * List_Add(struct list * list, size_t * indexPtr);
bool List_AddCopy(struct list * list, void * value, size_t * indexPtr);


bool List_RemoveFast(struct list * list, size_t index, void * swapSpace);

#define LIST_CREATE_TYPE(name, type)                                           \
                                                                               \
struct list_ ## name                                                           \
{                                                                              \
   struct list list;                                                           \
};                                                                             \
                                                                               \
static inline                                                                  \
void List_Init_ ## name (struct list_ ## name * list,                          \
                         size_t numberOfElements,                              \
                         type * data)                                          \
{                                                                              \
   List_Init(&list->list, sizeof(type), numberOfElements, data);               \
}                                                                              \
                                                                               \
static inline                                                                  \
type * List_AddPtr_ ## name (struct list_ ## name * list,                      \
                            size_t * indexPtr)                                 \
{                                                                              \
   return List_Add(&list->list, indexPtr);                                     \
}                                                                              \
                                                                               \
static inline                                                                  \
bool List_Add_ ## name (struct list_ ## name * list,                           \
                        type value,                                            \
                        size_t * indexPtr)                                     \
{                                                                              \
   return List_AddCopy(&list->list, &value, indexPtr);                         \
}                                                                              \
                                                                               \
static inline                                                                  \
bool List_AddCopy_ ## name(struct list_ ## name * list,                        \
                           type * value,                                       \
                           size_t * indexPtr)                                  \
{                                                                              \
   return List_AddCopy(&list->list, value, indexPtr);                          \
}                                                                              \
                                                                               \
static inline                                                                  \
type * List_GetPtr_ ## name (struct list_ ## name * list, size_t index)        \
{                                                                              \
   return ((type*)list->list.base) + index;                                    \
}                                                                              \
                                                                               \
static inline                                                                  \
size_t List_GetCount_ ## name (const struct list_ ## name * list)              \
{                                                                              \
   return list->list.count;                                                    \
}                                                                              \
                                                                               \
static inline                                                                  \
type * List_Get_ ## name (const struct list_ ## name * list,                   \
                          size_t * count)                                      \
{                                                                              \
   if(count != NULL) (*count) = list->list.count;                              \
   return list->list.base;                                                     \
}                                                                              \
                                                                               \
static inline                                                                  \
bool List_RemoveFast_ ## name (struct list_ ## name * list, size_t index)      \
{                                                                              \
   type swapSpace;                                                             \
   return List_RemoveFast(&list->list, index, &swapSpace);                     \
}

#endif // __LIST_H__
