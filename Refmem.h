#ifndef __REFMEM_H__
#define __REFMEM_H__
#include <stddef.h>
#include <stdbool.h>

struct refmementry
{
   size_t refCount;
   void * data;
};

struct refmem
{
   size_t size;
   size_t elementSize;
   size_t count;
   struct refmementry * entries;
   void * base;
};

void Refmem_Init(struct refmem * mem, size_t elementSize, 
                 size_t size, void * data, struct refmementry * entryData);
                 
void * Refmem_Alloc(struct refmem * mem);
void Refmem_Take(struct refmem * mem, void * ptr);
bool Refmem_Release(struct refmem * mem, void * ptr);

#define REFMEM_CREATE_TYPE(name, type)                                         \
struct refmem_ ## name                                                         \
{                                                                              \
   struct refmem mem;                                                          \
};                                                                             \
                                                                               \
static inline                                                                  \
void Refmem_Init_ ## name (struct refmem_ ## name * mem, size_t size,          \
                           type * data, struct refmementry * entryData)        \
{                                                                              \
   Refmem_Init(&mem->mem, sizeof(type), size, data, entryData);                \
}                                                                              \
                                                                               \
static inline                                                                  \
type * Refmem_Alloc_ ## name (struct refmem_ ## name * mem)                    \
{                                                                              \
   return Refmem_Alloc(&mem->mem);                                             \
}                                                                              \
                                                                               \
static inline                                                                  \
void Refmem_Take_ ## name (struct refmem_ ## name * mem, type * ptr)           \
{                                                                              \
   return Refmem_Take(&mem->mem, ptr);                                         \
}                                                                              \
                                                                               \
static inline                                                                  \
bool Refmem_Release_ ## name (struct refmem_ ## name * mem, type * ptr)        \
{                                                                              \
   return Refmem_Release(&mem->mem, ptr);                                      \
}


#endif // __REFMEM_H__
