#ifndef __EVENT_H__
#define __EVENT_H__
#include "Queue.h"

struct ship;

enum eventType
{
   eET_None,
   eET_ShipAttack,
   eET_ShipDestroyed,
};

struct eventShipAttack
{
   enum eventType type;
   struct ship * attacker;
   struct ship * target;
   unsigned int dammage;
};

struct eventShipDestroyed
{
   enum eventType type;
   struct ship * ship;
};

union event
{
   enum eventType            type;
   struct eventShipAttack    shipAttack;
   struct eventShipDestroyed shipDestroyed;
};

QUEUE_CREATE_TYPE(event, union event * )

void Event_Init(void);
union event * Event_Create(enum eventType type);
union event * Event_CreateShipAttack(struct ship * attacker, 
                                     struct ship * target,
                                     unsigned int dammage);
union event * Event_CreateShipDestroyed(struct ship * ship);
void Event_Take(union event * event);
bool Event_Release(union event * event);

#endif // __EVENT_H__

