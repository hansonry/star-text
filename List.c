#include <string.h>
#include "List.h"

#define LIST_GETPTR(list, index) \
   (((unsigned char*)(list)->base) + ((index) * (list)->elementSize))

void List_Init(struct list * list, 
               size_t elementSize, 
               size_t numberOfElements, 
               void * data)
{
   list->base = data;
   list->size = numberOfElements;
   list->count = 0;
   list->elementSize = elementSize;
}

static inline
size_t List_GetRemainingSpace(const struct list * list)
{
   return list->size - list->count;
}

void * List_Add(struct list * list, size_t * indexPtr)
{
   size_t index = 0;
   if(List_GetRemainingSpace(list) <= 0)
   {
      return NULL;
   }
   
   index = list->count;
   
   if(indexPtr != NULL)
   {
      (*indexPtr) = index;
   }
   
   list->count ++;
   return LIST_GETPTR(list, index);
}

bool List_AddCopy(struct list * list, void * value, size_t * indexPtr)
{
   void * ptr = List_Add(list, indexPtr);
   if(ptr == NULL)
   {
      return false;
   }
   memcpy(ptr, value, list->elementSize);
   return true;
}

static inline
void List_SwapInternal(struct list * list, size_t index1, size_t index2, 
                       void * swapSpace)
{
   if(index1 != index2)
   {
      void * ptr1 = LIST_GETPTR(list, index1);
      void * ptr2 = LIST_GETPTR(list, index2);
      memcpy(swapSpace, ptr1,      list->elementSize);
      memcpy(ptr1,      ptr2,      list->elementSize);
      memcpy(ptr2,      swapSpace, list->elementSize);
   }
}
static inline
void List_MoveInternal(struct list * list, size_t index1, size_t index2)
{
   if(index1 != index2)
   {
      void * ptr1 = LIST_GETPTR(list, index1);
      void * ptr2 = LIST_GETPTR(list, index2);
      memcpy(ptr1, ptr2, list->elementSize);
   }
}


bool List_RemoveFast(struct list * list, size_t index, void * swapSpace)
{
   if(index >= list->count)
   {
      return false;
   }
   list->count --;
   List_SwapInternal(list, index, list->count, swapSpace);   
   return true;
}

