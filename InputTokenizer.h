#ifndef __INPUTTOKENIZER_H__
#define __INPUTTOKENIZER_H__
#include "List.h"
#include "Queue.h"
#include "StringBuilder.h"

QUEUE_CREATE_TYPE(char, char)


enum inputTokenType
{
   eITT_None,
   eITT_Text,
   eITT_Integer,
   eITT_Slash,
   eITT_String,
   eITT_EndOfCommand,
};

struct inputToken
{
   enum inputTokenType type;
   char * text;
   int intValue;
};

QUEUE_CREATE_TYPE(inputToken, struct inputToken)

struct inputTokenizerState
{
   void (*state)(struct inputTokenizerState * s);
   struct queue_inputToken * tokenList;
   struct queue_char * input;
   struct inputToken token;
   struct stringBuilder stringBuilder;
};


void InputTokenizer_Init(struct inputTokenizerState * state, 
                         struct queue_inputToken * tokenList, 
                         struct queue_char * input);
void InputTokenizer_Free(struct inputTokenizerState * state);


void InputTokenizer_Tokenize(struct inputTokenizerState * state);

void InputTokenizer_FreeToken(struct inputToken * token);


#endif // __INPUTTOKENIZER_H__

