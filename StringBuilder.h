#ifndef __STRINGBUILDER_H__
#define __STRINGBUILDER_H__
#include <stddef.h>


struct stringBuilder
{
   char * base;
   size_t count;
   size_t size;
   size_t growBy;
};


void StringBuilder_Init(struct stringBuilder * builder, size_t growBy);
void StringBuilder_Free(struct stringBuilder * builder);

void StringBuilder_Add(struct stringBuilder * builder, const char * chars, 
                       size_t size);
void StringBuilder_Clear(struct stringBuilder * builder);

char * StringBuilder_Duplicate(const struct stringBuilder * builder);

static inline
void StringBuilder_AddOne(struct stringBuilder * builder, char c)
{
   StringBuilder_Add(builder, &c, 1);
}

static inline
size_t StringBuilder_GetCount(const struct stringBuilder * builder)
{
   return builder->count;
}

static inline
char * StringBuilder_DuplicateAndClear(struct stringBuilder * builder)
{
   char * out = StringBuilder_Duplicate(builder);
   StringBuilder_Clear(builder);
   return out;
}


#endif // __STRINGBUILDER_H__

