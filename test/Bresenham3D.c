#include <ryanmock.h>
#include <Bresenham3D.h>


void test_StartAndEndAtSamePlace(void)
{
   struct bresenham3d b3d;
   struct position start = {0, 0, 0};
   struct position end   = {0, 0 ,0};

   Bresenham3D_Init(&b3d, &start, &end);
   
   rmmAssertFalse(Bresenham3D_Next(&b3d, NULL));

}

#define GET_SIZE(array) sizeof(array)/sizeof(*array)



void test_OneAway(void)
{
   struct bresenham3d b3d;
   struct position start = {0, 0, 0};
   struct position ends[26] = {
      {-1,  -1 , -1},
      {-1,  -1 ,  0},
      {-1,  -1 ,  1},
      {-1,   0 , -1},
      {-1,   0 ,  0},
      {-1,   0 ,  1},
      {-1,   1 , -1},
      {-1,   1 ,  0},
      {-1,   1 ,  1},
      { 0,  -1 , -1},
      { 0,  -1 ,  0},
      { 0,  -1 ,  1},
      { 0,   0 , -1},
      { 0,   0 ,  1},
      { 0,   1 , -1},
      { 0,   1 ,  0},
      { 0,   1 ,  1},
      { 1,  -1 , -1},
      { 1,  -1 ,  0},
      { 1,  -1 ,  1},
      { 1,   0 , -1},
      { 1,   0 ,  0},
      { 1,   0 ,  1},
      { 1,   1 , -1},
      { 1,   1 ,  0},
      { 1,   1 ,  1},
   };
   int i;

   for(i = 0; i < 26; i++)
   {
      struct position out;

      rmmStackPushHere(&out, "Testing outs[%d] {%d, %d, %d}", 
                              i, ends[i].x, ends[i].y, ends[i].z);
      Bresenham3D_Init(&b3d, &start, &ends[i]);
   
      rmmAssertTrue(Bresenham3D_Next(&b3d, &out));
     
      rmmAssertIntEqual(out.x, ends[i].x);
      rmmAssertIntEqual(out.y, ends[i].y);
      rmmAssertIntEqual(out.z, ends[i].z);

      rmmAssertFalse(Bresenham3D_Next(&b3d, NULL));
      rmmStackPop(&out);
   }

}


void test_LowLine(void)
{
   struct bresenham3d b3d;
   struct position start = {0, 0, 0};
   struct position end   = {3, 1, 0};
   struct position points[] = {
      { 1, 0, 0 },
      { 2, 1, 0 },
      { 3, 1, 0 },
   };
   int i;

   Bresenham3D_Init(&b3d, &start, &end);

   for(i = 0; i < GET_SIZE(points); i++)
   {
      struct position out;

      rmmStackPushHere(&out, "Testing points[%d] {%d, %d, %d}", 
                              i, points[i].x, points[i].y, points[i].z);
   
      rmmAssertTrue(Bresenham3D_Next(&b3d, &out));
     
      rmmAssertIntEqual(out.x, points[i].x);
      rmmAssertIntEqual(out.y, points[i].y);
      rmmAssertIntEqual(out.z, points[i].z);

      rmmStackPop(&out);
   }
   rmmAssertFalse(Bresenham3D_Next(&b3d, NULL));

}

void test_HighLine(void)
{
   struct bresenham3d b3d;
   struct position start = {0, 0, 0};
   struct position end   = {1, 3, 0};
   struct position points[] = {
      { 0, 1, 0 },
      { 1, 2, 0 },
      { 1, 3, 0 },
   };
   int i;

   Bresenham3D_Init(&b3d, &start, &end);

   for(i = 0; i < GET_SIZE(points); i++)
   {
      struct position out;

      rmmStackPushHere(&out, "Testing points[%d] {%d, %d, %d}", 
                              i, points[i].x, points[i].y, points[i].z);
   
      rmmAssertTrue(Bresenham3D_Next(&b3d, &out));
     
      rmmAssertIntEqual(out.x, points[i].x);
      rmmAssertIntEqual(out.y, points[i].y);
      rmmAssertIntEqual(out.z, points[i].z);

      rmmStackPop(&out);
   }
   rmmAssertFalse(Bresenham3D_Next(&b3d, NULL));

}


int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_StartAndEndAtSamePlace),
      rmmMakeTest(test_OneAway),
      rmmMakeTest(test_LowLine),
      rmmMakeTest(test_HighLine),
   };
   return rmmRunTestsCmdLine(tests, "Bresenham3D", argc, args);
}


