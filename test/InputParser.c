#include <ryanmock.h>
#include <InputParser.h>


void test_Move(void)
{
   union command cmd;
   rmmAssertTrue(InputParser_Parse("move 1 2 3", &cmd));

   rmmAssertEnumEqual(cmd.type, eCT_Move);
}



int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_Move),
   };
   return rmmRunTestsCmdLine(tests, "InputParser", argc, args);
}


