#include <ryanmock.h>
#include <List.h>

LIST_CREATE_TYPE(int, int)


void test_AddCopy(void)
{
   int list_data[5];
   struct list_int list;
   int * ptr; 
   size_t index;

   List_Init_int(&list, 5, list_data);

   rmmAssertIntEqual(List_GetCount_int(&list), 0);
   
   ptr = List_AddPtr_int(&list, &index);
   rmmAssertPtrNotNULL(ptr);
   (*ptr) = 5;
   rmmAssertSizeTEqual(index, 0);
   rmmAssertIntEqual(List_GetCount_int(&list), 1);

   ptr = List_AddPtr_int(&list, &index);
   rmmAssertPtrNotNULL(ptr);
   (*ptr) = 6;
   rmmAssertSizeTEqual(index, 1);
   rmmAssertIntEqual(List_GetCount_int(&list), 2);
   
   ptr = List_GetPtr_int(&list, 0);

   rmmAssertIntEqual(ptr[0], 5);
   rmmAssertIntEqual(ptr[1], 6);
}

void test_DontAddWhenFull(void)
{
   int list_data[5];
   struct list_int list;
   size_t index = 15;

   List_Init_int(&list, 5, list_data);
   rmmAssertPtrEqual(List_AddPtr_int(&list, NULL), &list_data[0]);
   rmmAssertPtrEqual(List_AddPtr_int(&list, NULL), &list_data[1]);
   rmmAssertPtrEqual(List_AddPtr_int(&list, NULL), &list_data[2]);
   rmmAssertPtrEqual(List_AddPtr_int(&list, NULL), &list_data[3]);
   rmmAssertPtrEqual(List_AddPtr_int(&list, NULL), &list_data[4]);
   rmmAssertPtrNULL(List_AddPtr_int(&list, &index));
   rmmAssertSizeTEqual(index, 15); // Index unchanged
   rmmAssertIntEqual(List_GetCount_int(&list), 5);
}

void test_Add(void)
{
   int list_data[5];
   struct list_int list;
   size_t index = 15;

   List_Init_int(&list, 5, list_data);
   rmmAssertTrue(List_Add_int(&list, 6, &index));
   rmmAssertSizeTEqual(index, 0); // Index unchanged
   rmmAssertIntEqual(List_GetCount_int(&list), 1);
   rmmAssertIntEqual(list_data[0], 6);
}

void test_RemoveFast(void)
{
   int list_data[5];
   struct list_int list;

   List_Init_int(&list, 5, list_data);
   
   rmmAssertFalse(List_RemoveFast_int(&list, 0));
   
   List_Add_int(&list, 5, NULL);
   rmmAssertIntEqual(List_GetCount_int(&list), 1);
   
   rmmAssertTrue(List_RemoveFast_int(&list, 0));
   rmmAssertIntEqual(List_GetCount_int(&list), 0);
   
   
   List_Add_int(&list, 1, NULL); 
   List_Add_int(&list, 2, NULL);
   List_Add_int(&list, 3, NULL);
   rmmAssertIntEqual(List_GetCount_int(&list), 3);
   
   rmmAssertTrue(List_RemoveFast_int(&list, 2));
   rmmAssertTrue(List_RemoveFast_int(&list, 0));
   rmmAssertIntEqual(List_GetCount_int(&list), 1);
   
   rmmAssertIntEqual(list_data[0], 2);
}


int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_AddCopy),
      rmmMakeTest(test_DontAddWhenFull),
      rmmMakeTest(test_Add),
      rmmMakeTest(test_RemoveFast),
   };
   return rmmRunTestsCmdLine(tests, "List", argc, args);
}


