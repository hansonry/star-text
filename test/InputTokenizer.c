#include <ryanmock.h>
#include <InputTokenizer.h>


static inline
void util_enqueueString(struct queue_char * input, const char * string)
{
   Queue_EnqueueCopy_char(input, string, strlen(string));
}

struct testData
{
   struct inputTokenizerState state;
   struct queue_inputToken output;
   struct queue_char input;
   struct inputToken * outputData;
   size_t outputDataSize;
   char * inputData;
   size_t inputDataSize;
};

static inline
void SetupTestData(struct testData * data, struct inputToken * outputData, 
                                           size_t outputDataSize,
                                           char * inputData,
                                           size_t inputDataSize)
{
   // Set Variables
   data->outputData     = outputData;
   data->outputDataSize = outputDataSize;
   data->inputData      = inputData;
   data->inputDataSize  = inputDataSize;

   // Initalize strucutes
   Queue_Init_inputToken(&data->output, data->outputDataSize, data->outputData);
   Queue_Init_char(&data->input, data->inputDataSize, data->inputData);

   InputTokenizer_Init(&data->state, &data->output, &data->input);
}


#define AssertAndPopToken(output, expectedTokenType, expectedText, expectedIntValue) \
rmmEnterFunction(AssertAndPopToken);                                                 \
_AssertAndPopToken(output, expectedTokenType, expectedText, expectedIntValue);       \
rmmExitFunction(AssertAndPopToken)

static inline
void _AssertAndPopToken(struct queue_inputToken * output, 
                        enum inputTokenType expectedTokenType, 
                        const char * expectedText,
                        int expectedIntValue)
{
   struct inputToken token;
   rmmAssertSizeTGreaterThanOrEqualTo(Queue_GetUsedCount_inputToken(output), 1);

   Queue_PeekCopy_inputToken(output, &token);
   Queue_Dequeue_inputToken(output);

   rmmAssertEnumEqual(token.type,    expectedTokenType);
   rmmAssertStringEqual(token.text,  expectedText);
   rmmAssertIntEqual(token.intValue, expectedIntValue);

   InputTokenizer_FreeToken(&token);
}

void test_Move(void)
{
   struct testData data;
   struct inputToken outputData[10];
   char inputData[10];
   const char * string = "move ";

   SetupTestData(&data, outputData, 10, inputData, 10);
   util_enqueueString(&data.input, string);

   InputTokenizer_Tokenize(&data.state);

   AssertAndPopToken(&data.output, eITT_Text, "move", 0);

   rmmAssertSizeTEqual(Queue_GetUsedCount_inputToken(&data.output), 0);

   InputTokenizer_Free(&data.state);

}

void test_Attack(void)
{
   struct testData data;
   struct inputToken outputData[10];
   char inputData[10];
   const char * string = "attack ";
   
   SetupTestData(&data, outputData, 10, inputData, 10);
   util_enqueueString(&data.input, string);

   InputTokenizer_Tokenize(&data.state);

   AssertAndPopToken(&data.output, eITT_Text, "attack", 0);

   rmmAssertSizeTEqual(Queue_GetUsedCount_inputToken(&data.output), 0);
   
   InputTokenizer_Free(&data.state);
}

void test_SpaceMove(void)
{
   struct testData data;
   struct inputToken outputData[10];
   char inputData[10];
   const char * string = " move ";
   
   SetupTestData(&data, outputData, 10, inputData, 10);
   util_enqueueString(&data.input, string);

   InputTokenizer_Tokenize(&data.state);

   AssertAndPopToken(&data.output, eITT_Text, "move", 0);

   rmmAssertSizeTEqual(Queue_GetUsedCount_inputToken(&data.output), 0);
   InputTokenizer_Free(&data.state);
}

void test_MoveSpace(void)
{
   struct testData data;
   struct inputToken outputData[10];
   char inputData[10];
   const char * string = "move   ";
   
   SetupTestData(&data, outputData, 10, inputData, 10);
   util_enqueueString(&data.input, string);

   InputTokenizer_Tokenize(&data.state);

   AssertAndPopToken(&data.output, eITT_Text, "move", 0);

   rmmAssertSizeTEqual(Queue_GetUsedCount_inputToken(&data.output), 0);
   InputTokenizer_Free(&data.state);
}

void test_MoveSpaceAttack(void)
{
   struct testData data;
   struct inputToken outputData[10];
   char inputData[20];
   const char * string = "move attack ";

   SetupTestData(&data, outputData, 10, inputData, 20);
   util_enqueueString(&data.input, string);

   InputTokenizer_Tokenize(&data.state);

   AssertAndPopToken(&data.output, eITT_Text, "move",   0);
   AssertAndPopToken(&data.output, eITT_Text, "attack", 0);

   rmmAssertSizeTEqual(Queue_GetUsedCount_inputToken(&data.output), 0);
   InputTokenizer_Free(&data.state);
}


void test_Integer1(void)
{
   
   struct testData data;
   struct inputToken outputData[10];
   char inputData[20];
   const char * string = "1 ";

   SetupTestData(&data, outputData, 10, inputData, 20);
   util_enqueueString(&data.input, string);

   InputTokenizer_Tokenize(&data.state);

   AssertAndPopToken(&data.output, eITT_Integer, "1", 1);
   
   rmmAssertSizeTEqual(Queue_GetUsedCount_inputToken(&data.output), 0);
   InputTokenizer_Free(&data.state);
}

void test_IntegerNeg1(void)
{
   struct testData data;
   struct inputToken outputData[10];
   char inputData[20];
   const char * string = "-1 ";
   
   SetupTestData(&data, outputData, 10, inputData, 20);
   util_enqueueString(&data.input, string);

   InputTokenizer_Tokenize(&data.state);

   AssertAndPopToken(&data.output, eITT_Integer, "-1", -1);
   
   rmmAssertSizeTEqual(Queue_GetUsedCount_inputToken(&data.output), 0);
   InputTokenizer_Free(&data.state);
}


void test_IntegerMoveInteger(void)
{
   struct testData data;
   struct inputToken outputData[10];
   char inputData[20];
   const char * string = "-4 move 13 ";
   SetupTestData(&data, outputData, 10, inputData, 20);
   util_enqueueString(&data.input, string);

   InputTokenizer_Tokenize(&data.state);

   AssertAndPopToken(&data.output, eITT_Integer, "-4",   -4);
   AssertAndPopToken(&data.output, eITT_Text,    "move", 0);
   AssertAndPopToken(&data.output, eITT_Integer, "13",   13);
   
   rmmAssertSizeTEqual(Queue_GetUsedCount_inputToken(&data.output), 0);
   InputTokenizer_Free(&data.state);
}

void test_SlashMove(void)
{
   struct testData data;
   struct inputToken outputData[10];
   char inputData[20];
   const char * string = "/move ";

   SetupTestData(&data, outputData, 10, inputData, 20);
   util_enqueueString(&data.input, string);

   InputTokenizer_Tokenize(&data.state);
   
   AssertAndPopToken(&data.output, eITT_Slash, "/", 0);
   AssertAndPopToken(&data.output, eITT_Text,  "move", 0);
   
   rmmAssertSizeTEqual(Queue_GetUsedCount_inputToken(&data.output), 0);
   InputTokenizer_Free(&data.state);
}

void test_MoveString(void)
{
   struct testData data;
   struct inputToken outputData[10];
   char inputData[20];
   const char * string = "move \"Hi Mom\"";
   
   SetupTestData(&data, outputData, 10, inputData, 20);
   util_enqueueString(&data.input, string);

   InputTokenizer_Tokenize(&data.state);

   AssertAndPopToken(&data.output, eITT_Text,   "move",   0);
   AssertAndPopToken(&data.output, eITT_String, "Hi Mom", 0);
   
   rmmAssertSizeTEqual(Queue_GetUsedCount_inputToken(&data.output), 0);
   InputTokenizer_Free(&data.state);

}

void test_StringWithQuotes(void)
{
   struct testData data;
   struct inputToken outputData[10];
   char inputData[30]; 
   const char * string = "\"You \\\"are\\\" special\"";
   SetupTestData(&data, outputData, 10, inputData, 30);
   util_enqueueString(&data.input, string);

   InputTokenizer_Tokenize(&data.state);

   AssertAndPopToken(&data.output, eITT_String, "You \"are\" special", 0);
   
   rmmAssertSizeTEqual(Queue_GetUsedCount_inputToken(&data.output), 0);
   
   InputTokenizer_Free(&data.state);
}

void test_Empty(void)
{
   struct testData data;
   struct inputToken outputData[10];
   char inputData[10];
   const char * string = "";

   SetupTestData(&data, outputData, 10, inputData, 10);
   util_enqueueString(&data.input, string);

   InputTokenizer_Tokenize(&data.state);

   rmmAssertSizeTEqual(Queue_GetUsedCount_inputToken(&data.output), 0);

   InputTokenizer_Free(&data.state);
}

void test_Space(void)
{
   struct testData data;
   struct inputToken outputData[10];
   char inputData[10];
   const char * string = "  ";

   SetupTestData(&data, outputData, 10, inputData, 10);
   util_enqueueString(&data.input, string);

   InputTokenizer_Tokenize(&data.state);

   rmmAssertSizeTEqual(Queue_GetUsedCount_inputToken(&data.output), 0);

   InputTokenizer_Free(&data.state);
}

void test_Incomplete(void)
{
   struct testData data;
   struct inputToken outputData[10];
   char inputData[10];
   const char * string = "mov";

   SetupTestData(&data, outputData, 10, inputData, 10);
   util_enqueueString(&data.input, string);

   InputTokenizer_Tokenize(&data.state);

   rmmAssertSizeTEqual(Queue_GetUsedCount_inputToken(&data.output), 0);

   // Coplete the statment
   util_enqueueString(&data.input, "e ");
   InputTokenizer_Tokenize(&data.state);
   
   AssertAndPopToken(&data.output, eITT_Text, "move", 0);
   
   rmmAssertSizeTEqual(Queue_GetUsedCount_inputToken(&data.output), 0);

   InputTokenizer_Free(&data.state);
}

void test_EndOfCommand(void)
{
   struct testData data;
   struct inputToken outputData[10];
   char inputData[10];
   const char * string = "\n";

   SetupTestData(&data, outputData, 10, inputData, 10);
   util_enqueueString(&data.input, string);

   InputTokenizer_Tokenize(&data.state);

   AssertAndPopToken(&data.output, eITT_EndOfCommand, "\n", 0);
   
   rmmAssertSizeTEqual(Queue_GetUsedCount_inputToken(&data.output), 0);

   InputTokenizer_Free(&data.state);
}

void test_EndOfCommandAfterText(void)
{
   struct testData data;
   struct inputToken outputData[10];
   char inputData[10];
   const char * string = "move\n";

   SetupTestData(&data, outputData, 10, inputData, 10);
   util_enqueueString(&data.input, string);

   InputTokenizer_Tokenize(&data.state);

   AssertAndPopToken(&data.output, eITT_Text,         "move", 0);
   AssertAndPopToken(&data.output, eITT_EndOfCommand, "\n",   0);
   
   rmmAssertSizeTEqual(Queue_GetUsedCount_inputToken(&data.output), 0);

   InputTokenizer_Free(&data.state);
}


int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_Move),
      rmmMakeTest(test_Attack),
      rmmMakeTest(test_SpaceMove),
      rmmMakeTest(test_MoveSpace),
      rmmMakeTest(test_MoveSpaceAttack),
      rmmMakeTest(test_Integer1),
      rmmMakeTest(test_IntegerNeg1),
      rmmMakeTest(test_IntegerMoveInteger),
      rmmMakeTest(test_SlashMove),
      rmmMakeTest(test_MoveString),
      rmmMakeTest(test_StringWithQuotes),
      rmmMakeTest(test_Empty),
      rmmMakeTest(test_Space),
      rmmMakeTest(test_Incomplete),
      rmmMakeTest(test_EndOfCommand),
      rmmMakeTest(test_EndOfCommandAfterText),
   };
   return rmmRunTestsCmdLine(tests, "InputTokenizer", argc, args);
}


