#include <ryanmock.h>
#include <Refmem.h>

REFMEM_CREATE_TYPE(int, int)

void test_Init(void)
{
   size_t i;
   int mem_data[5];
   struct refmementry mem_entries[5];
   struct refmem_int mem;

   Refmem_Init_int(&mem, 5, mem_data, mem_entries);
   
   for(i = 0; i < 5; i++)
   {
      rmmAssertSizeTEqual(mem_entries[i].refCount, 0);
      rmmAssertPtrEqual(mem_entries[i].data, &mem_data[i]);
   }
}

void test_AllocSuccess(void)
{
   int mem_data[5];
   struct refmementry mem_entries[5];
   struct refmem_int mem;
   
   int * someNumber[2];
   size_t index[2];

   Refmem_Init_int(&mem, 5, mem_data, mem_entries);
   
   someNumber[0] = Refmem_Alloc_int(&mem);
   rmmAssertPtrNotNULL(someNumber[0]);
   
   index[0] = someNumber[0] - mem_data;
   rmmAssertSizeTEqual(mem_entries[index[0]].refCount, 1); 

   someNumber[1] = Refmem_Alloc_int(&mem);
   rmmAssertPtrNotNULL(someNumber[1]);
   
   index[1] = someNumber[1] - mem_data;
   rmmAssertSizeTEqual(mem_entries[index[1]].refCount, 1); 
   
   rmmAssertPtrNotEqual(someNumber[0], someNumber[1]);
   rmmAssertSizeTNotEqual(index[0], index[1]);
}

void test_AllocFailWhenFull(void)
{
   int mem_data[5];
   struct refmementry mem_entries[5];
   struct refmem_int mem;

   Refmem_Init_int(&mem, 5, mem_data, mem_entries);
   rmmAssertPtrNotNULL(Refmem_Alloc_int(&mem));
   rmmAssertPtrNotNULL(Refmem_Alloc_int(&mem));
   rmmAssertPtrNotNULL(Refmem_Alloc_int(&mem));
   rmmAssertPtrNotNULL(Refmem_Alloc_int(&mem));
   rmmAssertPtrNotNULL(Refmem_Alloc_int(&mem));
   
   rmmAssertPtrNULL(Refmem_Alloc_int(&mem));
}

void test_TakeSucess(void)
{
   int mem_data[5];
   struct refmementry mem_entries[5];
   struct refmem_int mem;
   
   int * someNumber;
   size_t index;

   Refmem_Init_int(&mem, 5, mem_data, mem_entries);
   
   someNumber = Refmem_Alloc_int(&mem);
   
   index = someNumber - mem_data;
   Refmem_Take_int(&mem, someNumber);   
   rmmAssertSizeTEqual(mem_entries[index].refCount, 2);

   Refmem_Take_int(&mem, someNumber);   
   rmmAssertSizeTEqual(mem_entries[index].refCount, 3);
   
}

void test_TakeFailWhenOutOfRange(void)
{
   int mem_data[5];
   struct refmementry mem_entries[5];
   struct refmem_int mem;
   int someNumberThatIsntInMem;
   size_t i;
   
   Refmem_Init_int(&mem, 5, mem_data, mem_entries);

   
   Refmem_Take_int(&mem, &someNumberThatIsntInMem);
   for(i = 0; i < 5; i++)
   {
      rmmAssertSizeTEqual(mem_entries[i].refCount, 0);
   }
}

void test_TakeFailWhenRefCountIs0(void)
{
   int mem_data[5];
   struct refmementry mem_entries[5];
   struct refmem_int mem;

   Refmem_Init_int(&mem, 5, mem_data, mem_entries);
   
   Refmem_Take_int(&mem, &mem_data[0]);
   rmmAssertSizeTEqual(mem_entries[0].refCount, 0);
}

void test_ReleaseSuccessWhenNotTaken(void)
{
   int mem_data[5];
   struct refmementry mem_entries[5];
   struct refmem_int mem;
   
   int * someNumber;
   size_t index;

   Refmem_Init_int(&mem, 5, mem_data, mem_entries);
   
   someNumber = Refmem_Alloc_int(&mem);
   rmmAssertTrue(Refmem_Release_int(&mem, someNumber));
   index = someNumber - mem_data;
   rmmAssertSizeTEqual(mem_entries[index].refCount, 0);
}

void test_ReleaseSuccessWhenTaken(void)
{
   int mem_data[5];
   struct refmementry mem_entries[5];
   struct refmem_int mem;
   
   int * someNumber;
   size_t index;

   Refmem_Init_int(&mem, 5, mem_data, mem_entries);
   
   someNumber = Refmem_Alloc_int(&mem);
   index = someNumber - mem_data;
   Refmem_Take_int(&mem, someNumber);
   rmmAssertFalse(Refmem_Release_int(&mem, someNumber));
   rmmAssertSizeTEqual(mem_entries[index].refCount, 1);
   
   rmmAssertTrue(Refmem_Release_int(&mem, someNumber));
}

void test_ReleaseFailWhenNotAlloced(void)
{
   int mem_data[5];
   struct refmementry mem_entries[5];
   struct refmem_int mem;

   Refmem_Init_int(&mem, 5, mem_data, mem_entries);
   
   rmmAssertFalse(Refmem_Release_int(&mem, &mem_data[0]));
   rmmAssertSizeTEqual(mem_entries[0].refCount, 0);
}

void test_ReleaseFailWhenOutOfRange(void)
{
   int mem_data[5];
   struct refmementry mem_entries[5];
   struct refmem_int mem;
   int someNumberThatIsntInMem;
   size_t i;
   
   Refmem_Init_int(&mem, 5, mem_data, mem_entries);

   
   rmmAssertFalse(Refmem_Release_int(&mem, &someNumberThatIsntInMem));
   for(i = 0; i < 5; i++)
   {
      rmmAssertSizeTEqual(mem_entries[i].refCount, 0);
   }
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_Init),
      rmmMakeTest(test_AllocSuccess),
      rmmMakeTest(test_AllocFailWhenFull),
      rmmMakeTest(test_TakeSucess),
      rmmMakeTest(test_TakeFailWhenOutOfRange),
      rmmMakeTest(test_TakeFailWhenRefCountIs0),
      rmmMakeTest(test_ReleaseSuccessWhenNotTaken),
      rmmMakeTest(test_ReleaseSuccessWhenTaken),
      rmmMakeTest(test_ReleaseFailWhenNotAlloced),
      rmmMakeTest(test_ReleaseFailWhenOutOfRange),
   };
   return rmmRunTestsCmdLine(tests, "Refmem", argc, args);
}


