#include <ryanmock.h>
#include <Simulation.h>


#define assertPositionEqual(pos, x, y, z) \
rmmEnterFunction(assertPositionEqual);    \
_assertPositionEqual(pos, x, y, z);       \
rmmExitFunction(assertPositionEqual)

static inline
void _assertPositionEqual(const struct position * pos, int x, int y, int z)
{
   rmmAssertIntEqual(pos->x, x);
   rmmAssertIntEqual(pos->y, y);
   rmmAssertIntEqual(pos->z, z);
}


void test_ShipMove(void)
{
   struct ship * ship;
   struct player * player;
   union command command;
   Simulation_Init();
   player = PlayerList_New();
   ship = ShipList_AddShip(player, 0, 0, 0);
   
   command.type = eCT_Move;
   Position_Set(&command.move.destination, 3, 0, 0);
   Simulation_CommandShip(ship, &command);

   assertPositionEqual(&ship->position, 0, 0, 0);

   Simulation_Step();
   assertPositionEqual(&ship->position, 1, 0, 0);

   Simulation_Step();
   assertPositionEqual(&ship->position, 2, 0, 0);
   
   Simulation_Step();
   assertPositionEqual(&ship->position, 3, 0, 0);
   
   Simulation_Step();
   assertPositionEqual(&ship->position, 3, 0, 0);

   Position_Set(&command.move.destination, 3, 3, 0);
   Simulation_CommandShip(ship, &command);
   
   Simulation_Step();
   assertPositionEqual(&ship->position, 3, 1, 0);

   Simulation_Step();
   assertPositionEqual(&ship->position, 3, 2, 0);
   
   Simulation_Step();
   assertPositionEqual(&ship->position, 3, 3, 0);
   
   Simulation_Step();
   assertPositionEqual(&ship->position, 3, 3, 0);
}

void test_ShipAttack(void)
{
   struct ship   * shipA,   * shipB;
   struct player * playerA, * playerB;
   union command command;
   union event * event;
   
   Simulation_Init();
   
   playerA = PlayerList_New();
   playerB = PlayerList_New();
   shipA = ShipList_AddShip(playerA, 0,  0, 0);
   shipB = ShipList_AddShip(playerB, 10, 0, 0);

   command.type = eCT_Target;
   command.target.target = shipB;
   
   Simulation_CommandShip(shipA, &command);
   
   command.type = eCT_Weapons;
   command.weapons.fireAtWill = true;

   Simulation_CommandShip(shipA, &command);

   rmmAssertUIntEqual(shipB->health, 100);
   
   Simulation_Step();
   rmmAssertUIntEqual(shipB->health, 55);
   
   Simulation_Step();
   rmmAssertUIntEqual(shipB->health, 55);
   Simulation_Step();
   rmmAssertUIntEqual(shipB->health, 55);

   Simulation_Step();
   rmmAssertUIntEqual(shipB->health, 10);
   
   event = PlayerList_EventDequeue(playerA);
   rmmAssertPtrNotNULL(event);
   rmmAssertEnumEqual(event->type, eET_ShipAttack);
   rmmAssertPtrEqual(event->shipAttack.attacker, shipA);
   rmmAssertPtrEqual(event->shipAttack.target,   shipB);
   rmmAssertFalse(Event_Release(event));

   event = PlayerList_EventDequeue(playerA);
   rmmAssertPtrNotNULL(event);
   rmmAssertEnumEqual(event->type, eET_ShipAttack);
   rmmAssertPtrEqual(event->shipAttack.attacker, shipA);
   rmmAssertPtrEqual(event->shipAttack.target,   shipB);
   rmmAssertFalse(Event_Release(event));

   event = PlayerList_EventDequeue(playerB);
   rmmAssertPtrNotNULL(event);
   rmmAssertEnumEqual(event->type, eET_ShipAttack);
   rmmAssertPtrEqual(event->shipAttack.attacker, shipA);
   rmmAssertPtrEqual(event->shipAttack.target,   shipB);
   rmmAssertTrue(Event_Release(event));

   event = PlayerList_EventDequeue(playerB);
   rmmAssertPtrNotNULL(event);
   rmmAssertEnumEqual(event->type, eET_ShipAttack);
   rmmAssertPtrEqual(event->shipAttack.attacker, shipA);
   rmmAssertPtrEqual(event->shipAttack.target,   shipB);
   rmmAssertTrue(Event_Release(event));
}

void test_ShipKill(void)
{
   struct ship   * shipA;
   struct player * playerA, * playerB;
   union event * event;
   
   Simulation_Init();
   
   playerA = PlayerList_New();
   playerB = PlayerList_New();
   shipA = ShipList_AddShip(playerA, 0,  0, 0);
   (void)ShipList_AddShip(playerB, 10, 0, 0);

   Ship_Kill(shipA);

   
   Simulation_Step();

   event = PlayerList_EventDequeue(playerA);
   rmmAssertPtrNotNULL(event);
   rmmAssertEnumEqual(event->type, eET_ShipDestroyed);
   rmmAssertPtrEqual(event->shipDestroyed.ship, shipA);
   rmmAssertFalse(Event_Release(event));

   event = PlayerList_EventDequeue(playerB);
   rmmAssertPtrNotNULL(event);
   rmmAssertEnumEqual(event->type, eET_ShipDestroyed);
   rmmAssertPtrEqual(event->shipDestroyed.ship, shipA);
   rmmAssertTrue(Event_Release(event));
}


int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_ShipMove),
      rmmMakeTest(test_ShipAttack),
      rmmMakeTest(test_ShipKill),
   };
   return rmmRunTestsCmdLine(tests, "Simulation", argc, args);
}


