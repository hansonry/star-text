#include <ryanmock.h>
#include <Queue.h>

QUEUE_CREATE_TYPE(int, int)


void test_Init(void)
{
   int data[5];
   struct queue_int queue;
   
   Queue_Init_int(&queue, 5, data);
   
   rmmAssertPtrEqual(queue.queue.base, data);
   rmmAssertSizeTEqual(queue.queue.count, 0);
   rmmAssertSizeTEqual(queue.queue.size, 5);
   rmmAssertSizeTEqual(queue.queue.elementSize, sizeof(int));
   rmmAssertSizeTEqual(queue.queue.head, 0);
   rmmAssertSizeTEqual(queue.queue.tail, 0);
}

void test_DequeueReturnsFalseWhenEmpty(void)
{
   int data[5];
   struct queue_int queue;
   
   Queue_Init_int(&queue, 5, data);

   rmmAssertFalse(Queue_Dequeue_int(&queue));
}

void test_QueueSomeNumbersAndDequeueThem(void)
{
   int data[5];
   struct queue_int queue;
   
   Queue_Init_int(&queue, 5, data);

   rmmAssertTrue(Queue_Enqueue_int(&queue, 1));
   rmmAssertTrue(Queue_Enqueue_int(&queue, 2));
   rmmAssertTrue(Queue_Enqueue_int(&queue, 3));
   rmmAssertTrue(Queue_Enqueue_int(&queue, 4));
   rmmAssertTrue(Queue_Enqueue_int(&queue, 5));
   rmmAssertFalse(Queue_Enqueue_int(&queue, 6));
   rmmAssertIntEqual(Queue_DequeueReturn_int(&queue), 1);
   rmmAssertIntEqual(Queue_DequeueReturn_int(&queue), 2);
   rmmAssertIntEqual(Queue_DequeueReturn_int(&queue), 3);
   rmmAssertIntEqual(Queue_DequeueReturn_int(&queue), 4);
   rmmAssertIntEqual(Queue_DequeueReturn_int(&queue), 5);
   rmmAssertFalse(Queue_NotEmpty_int(&queue));
   
   rmmAssertTrue(Queue_Enqueue_int(&queue, 6));
   rmmAssertTrue(Queue_Enqueue_int(&queue, 7));
   rmmAssertTrue(Queue_Enqueue_int(&queue, 8));
   rmmAssertTrue(Queue_Enqueue_int(&queue, 9));
   rmmAssertTrue(Queue_Enqueue_int(&queue, 10));
   rmmAssertFalse(Queue_Enqueue_int(&queue, 11));
   rmmAssertIntEqual(Queue_DequeueReturn_int(&queue), 6);
   rmmAssertIntEqual(Queue_DequeueReturn_int(&queue), 7);
   rmmAssertIntEqual(Queue_DequeueReturn_int(&queue), 8);
   rmmAssertIntEqual(Queue_DequeueReturn_int(&queue), 9);
   rmmAssertIntEqual(Queue_DequeueReturn_int(&queue), 10);
   rmmAssertFalse(Queue_NotEmpty_int(&queue));
}

void test_EnqueueCopy1(void)
{
   int queueData[5];
   int data = 6;
   struct queue_int queue;
   
   Queue_Init_int(&queue, 5, queueData);

   rmmAssertSizeTEqual(Queue_EnqueueCopy_int(&queue, &data, 1), 1);

   rmmAssertSizeTEqual(Queue_GetUsedCount_int(&queue), 1);
   rmmAssertIntEqual(Queue_Peek_int(&queue), 6);
}

void test_EnqueueCopyFullOffset0(void)
{
   int queueData[5];
   int data[5] = { 6, 7, -4, 5, 88 };
   struct queue_int queue;
   
   Queue_Init_int(&queue, 5, queueData);

   rmmAssertSizeTEqual(Queue_EnqueueCopy_int(&queue, data, 5), 5);

   rmmAssertSizeTEqual(Queue_GetUsedCount_int(&queue), 5);
   rmmAssertMemoryEqual(queueData, data, 5);
}

void test_EnqueueCopyOverFullOffset0(void)
{
   int queueData[5];
   int data[6] = { 6, 7, -4, 5, 88, 77 };
   struct queue_int queue;
   
   Queue_Init_int(&queue, 5, queueData);

   rmmAssertSizeTEqual(Queue_EnqueueCopy_int(&queue, data, 6), 5);

   rmmAssertSizeTEqual(Queue_GetUsedCount_int(&queue), 5);
   rmmAssertMemoryEqual(queueData, data, 5);
}

void test_EnqueueCopyFullOffset2(void)
{
   int queueData[5];
   int data[5] = { 6, 7, -4, 5, 88 };
   int expectedData[5] = { 5, 88, 6, 7, -4 };
   struct queue_int queue;
   
   Queue_Init_int(&queue, 5, queueData);

   queue.queue.head = queue.queue.tail = 2;

   rmmAssertSizeTEqual(Queue_EnqueueCopy_int(&queue, data, 5), 5);

   rmmAssertSizeTEqual(Queue_GetUsedCount_int(&queue), 5);
   rmmAssertMemoryEqual(queueData, expectedData, 5);
}


int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_Init),
      rmmMakeTest(test_DequeueReturnsFalseWhenEmpty),
      rmmMakeTest(test_QueueSomeNumbersAndDequeueThem),
      rmmMakeTest(test_EnqueueCopy1),
      rmmMakeTest(test_EnqueueCopyFullOffset0),
      rmmMakeTest(test_EnqueueCopyOverFullOffset0),
      rmmMakeTest(test_EnqueueCopyFullOffset2),
   };
   return rmmRunTestsCmdLine(tests, "Queue", argc, args);
}


