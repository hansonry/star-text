#include <stdlib.h>
#include "Bresenham3D.h"



static inline
int IntSign(int num)
{
   return (num > 0) - (num < 0);
}


void Bresenham3D_Init(struct bresenham3d * b3d,
                      const struct position * start, 
                      const struct position * end)
{
   enum positionComponent largestComp;
   struct position diff, diffAbs;
   Position_Copy(&diff, end);
   Position_Subtract(&diff, start);

   diffAbs.x = abs(diff.x);
   diffAbs.y = abs(diff.y);
   diffAbs.z = abs(diff.z);
   
   // Initalize Mix
   b3d->mix[1] = ePC_Y;
   b3d->mix[2] = ePC_Z;

   // Find Biggest Difference
   largestComp = Position_GetLargestComponent(&diffAbs);

   // Swap X with largest component in mix
   b3d->mix[largestComp] = ePC_X;
   b3d->mix[0] = largestComp;

   // Create Mixed Diff
   Position_Copy(&b3d->mixedDiffs, &diff);
   Position_Map(&b3d->mixedDiffs, b3d->mix);

   // Initialize epsilons
   Position_Set(&b3d->mixedEpsilon, 0, 0, 0);

   // Copy over initial Position_Set
   Position_Copy(&b3d->mixedPosition, start);
   Position_Map(&b3d->mixedPosition, b3d->mix);

   // Compute End
   b3d->end = Position_GetComponent(end, b3d->mix[0]);

   // Compute Mixed Sign
   b3d->mixedSign.x = IntSign(diff.x);
   b3d->mixedSign.y = IntSign(diff.y);
   b3d->mixedSign.z = IntSign(diff.z);
   Position_Map(&b3d->mixedSign, b3d->mix);
   
}

bool Bresenham3D_Next(struct bresenham3d * b3d,
                      struct position * next)
{
   struct position deltaAbs;
   if(b3d->mixedPosition.x == b3d->end)
   {
      return false;
   }

   // Compute ABS
   deltaAbs.x = b3d->mixedDiffs.x * b3d->mixedSign.x;
   deltaAbs.y = b3d->mixedDiffs.y * b3d->mixedSign.y;
   deltaAbs.z = b3d->mixedDiffs.z * b3d->mixedSign.z;

   // Mixed X adjustment
   b3d->mixedPosition.x += b3d->mixedSign.x;

   // Update Epsilons
   b3d->mixedEpsilon.y += deltaAbs.y;
   b3d->mixedEpsilon.z += deltaAbs.x;

   // Mixed Y adjustment
   if((b3d->mixedEpsilon.y << 1) >= deltaAbs.x)
   {
      b3d->mixedPosition.y += b3d->mixedSign.y;
      b3d->mixedEpsilon.y -= deltaAbs.x;
   }

   // Mixed Z adjustment
   if((b3d->mixedEpsilon.z << 1) >= b3d->mixedDiffs.x)
   {
      b3d->mixedPosition.z += b3d->mixedSign.z;
      b3d->mixedEpsilon.z -= deltaAbs.x;
   }

   // Fill next pointer with unmapped value
   if(next != NULL)
   {
      Position_Copy(next, &b3d->mixedPosition);
      Position_Unmap(next, b3d->mix);
   }

   return true;
}


