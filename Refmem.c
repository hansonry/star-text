#include "Refmem.h"

static inline
void Refmem_InitRefs(struct refmem * mem)
{
   size_t i;
   for(i = 0; i < mem->size; i ++)
   {
      struct refmementry * entry = &mem->entries[i];
      entry->refCount = 0;
      entry->data = ((unsigned char*)mem->base) + (i * mem->elementSize);
   }
}

void Refmem_Init(struct refmem * mem, size_t elementSize, 
                 size_t size, void * data, struct refmementry * entryData)
{
   mem->elementSize = elementSize;
   mem->size = size;
   mem->count = 0;
   mem->entries = entryData;
   mem->base = data;
   Refmem_InitRefs(mem);
}

static inline
struct refmementry * Refmem_FindNextFree(struct refmem * mem)
{
   size_t i;
   for(i = 0; i < mem->size; i ++)
   {
      struct refmementry * entry = &mem->entries[i];
      if(entry->refCount == 0)
      {
         return entry;
      }
   }
   return NULL;
}


void * Refmem_Alloc(struct refmem * mem)
{
   if(mem->count < mem->size)
   {
      struct refmementry * entry = Refmem_FindNextFree(mem);
      entry->refCount ++;
      mem->count ++;
      return entry->data;
   }
   return NULL;
}

static inline
bool Refmem_IsInRange(struct refmem * mem, void * ptr, size_t * index)
{
   size_t offset;
   size_t lIndex;
   if(ptr < mem->base)
   {
      return false;
   }
   offset = ptr - mem->base;
   if((offset % mem->elementSize) != 0)
   {
      return false;
   }
   lIndex = offset / mem->elementSize;
   if(lIndex >= mem->size)
   {
      return false;
   }
   if(index != NULL)
   {
      (*index) = lIndex;
   }
   return true;
}   

void Refmem_Take(struct refmem * mem, void * ptr)
{
   size_t index;
   if(Refmem_IsInRange(mem, ptr, &index))
   {
      struct refmementry * entry = &mem->entries[index];
      if(entry->refCount > 0)
      {
         entry->refCount ++;
      }
   }
}

bool Refmem_Release(struct refmem * mem, void * ptr)
{
   size_t index;
   bool deleted = false;
   if(Refmem_IsInRange(mem, ptr, &index))
   {
      struct refmementry * entry = &mem->entries[index];
      if(entry->refCount == 1)
      {
         mem->count --;
         deleted = true;
      }
      if(entry->refCount > 0)
      {
         entry->refCount --;
      }
   }
   return deleted;
}
