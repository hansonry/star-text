#include <string.h>
#include "Queue.h"

#define BYTE_OFFSET_OF(base, offset) (((unsigned char*)(base)) + (offset))
#define BYTE_OFFSET_OF_CONST(base, offset) (((const unsigned char*)(base)) + (offset))

#define QUEUE_GETPTR(queue, index) \
BYTE_OFFSET_OF((queue)->base, (index) * (queue)->elementSize)

void Queue_Init(struct  queue * queue, size_t elementSize, 
                size_t size, void * memory)
{
   queue->base        = memory;
   queue->elementSize = elementSize;
   queue->size        = size;
   queue->count       = 0;
   queue->head        = 0;
   queue->tail        = 0;
}


void * Queue_EnqueuePtr(struct queue * queue)
{
   size_t index;
   if(queue->count >= queue->size)
   {
      return NULL;
   }
   index = queue->tail;
   queue->tail  = (queue->tail + 1) % queue->size;
   queue->count ++;
   return QUEUE_GETPTR(queue, index);
}

static inline
size_t Queue_GetFreeCount(const struct queue * queue)
{
   return queue->size - queue->count;
}

static inline
size_t Queue_GetTailSpaceAtEnd(const struct queue * queue)
{
   return queue->size - queue->tail;
}

size_t Queue_EnqueueCopy(struct queue * queue, const void * value, size_t count)
{
   const size_t freeCount = Queue_GetFreeCount(queue);
   const size_t toCopy = count < freeCount ? count : freeCount;
   if(toCopy > 0)
   {
      const size_t tailSpace = Queue_GetTailSpaceAtEnd(queue);
      const size_t firstCopySize = toCopy <= tailSpace ? toCopy : tailSpace;
      
      void * dest = QUEUE_GETPTR(queue, queue->tail);
      memcpy(dest, value, queue->elementSize * firstCopySize);

      if(toCopy > firstCopySize)
      {
         const size_t secondCopySize = toCopy - firstCopySize;
         void * dest2 = QUEUE_GETPTR(queue, 0);
         const void * value2 = BYTE_OFFSET_OF_CONST(value, firstCopySize * queue->elementSize); 
         memcpy(dest2, value2, queue->elementSize * secondCopySize);
            
      }

      queue->tail = (queue->tail + toCopy) % queue->size;
      queue->count += toCopy;
   } 
   return toCopy;
}

void * Queue_PeekPtr(const struct queue * queue)
{
   if(queue->count == 0)
   {
      return NULL;
   }
   return QUEUE_GETPTR(queue, queue->head);
}

bool Queue_PeekCopy(const struct queue * queue, void * outValue)
{
   void * src = Queue_PeekPtr(queue);
   if(src == NULL)
   {
      return false;
   }
   memcpy(outValue, src, queue->elementSize);
   return true;
}

bool Queue_Dequeue(struct queue * queue)
{
   if(queue->count == 0)
   {
      return false;
   }
   queue->head  = (queue->head + 1) % queue->size;
   queue->count --;
   return true;
}

bool Queue_DequeueCopy(struct queue * queue, void * outValue)
{
   void * src = Queue_PeekPtr(queue);
   if(src == NULL)
   {
      return false;
   }

   memcpy(outValue, src, queue->elementSize);
   Queue_Dequeue(queue);
   return true;
}

void Queue_Clear(struct queue * queue)
{
   queue->count = 0;
   queue->head  = 0;
   queue->tail  = 0;
}

