#include "PlayerList.h"
#include "Refmem.h"

REFMEM_CREATE_TYPE(player, struct player)

#define MAX_PLAYER_COUNT 512

struct playerData
{
   struct player playerMemData[MAX_PLAYER_COUNT];
   struct refmementry playerMemEntries[MAX_PLAYER_COUNT];
   struct refmem_player playerMem;

   struct list_player playerList;
   struct player * playerListData[MAX_PLAYER_COUNT];
};

static struct playerData PlayerData;


void PlayerList_Init(void)
{
   List_Init_player(&PlayerData.playerList, MAX_PLAYER_COUNT, 
                                            PlayerData.playerListData);
   Refmem_Init_player(&PlayerData.playerMem, MAX_PLAYER_COUNT, 
                                             PlayerData.playerMemData, 
                                             PlayerData.playerMemEntries);
}


void PlayerList_Take(struct player * player)
{
   Refmem_Take_player(&PlayerData.playerMem, player);
}

bool PlayerList_Release(struct player * player)
{
   return Refmem_Release_player(&PlayerData.playerMem, player);
}

struct player * PlayerList_New(void)
{
   struct player * player = Refmem_Alloc_player(&PlayerData.playerMem);
   if(player != NULL)
   {
      Queue_Init_event(&player->eventQueue, 
                       PLAYER_EVENT_QUEUE_SIZE, player->eventData);
      List_Add_player(&PlayerData.playerList, player, NULL);
   }
   return player;
}

void PlayerList_EventQueue(struct player * player, union event * event)
{
   Event_Take(event);
   Queue_Enqueue_event(&player->eventQueue, event);
}

union event * PlayerList_EventDequeue(struct player * player)
{
   union event * event;
   if(Queue_DequeueCopy_event(&player->eventQueue, &event))
   {
      return event;
   }
   return NULL;
}




