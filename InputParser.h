#ifndef __COMMANDPARSER_H__
#define __COMMANDPARSER_H__
#include <stdbool.h>
#include "Command.h"

bool InputParser_Parse(const char * str, union command * command);


#endif // __COMMANDPARSER_H__

