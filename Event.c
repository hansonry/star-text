#include "Event.h"
#include "Refmem.h"
#include "ShipList.h"

#define EVENT_MEMORY_COUNT 256

REFMEM_CREATE_TYPE(event, union event)

struct eventData
{
   union event refmemData[EVENT_MEMORY_COUNT];
   struct refmementry refmemEntries[EVENT_MEMORY_COUNT];
   struct refmem_event refmem;
};

static struct eventData EventData;


void Event_Init(void)
{
   Refmem_Init_event(&EventData.refmem, EVENT_MEMORY_COUNT, 
                     EventData.refmemData, EventData.refmemEntries);
}

union event * Event_Create(enum eventType type)
{
   union event * event = Refmem_Alloc_event(&EventData.refmem);
   if(event == NULL)
   {
      return NULL;
   }
   event->type = type;
   return event;
}

union event * Event_CreateShipAttack(struct ship * attacker, 
                                     struct ship * target, unsigned int dammage)
{
   union event * event = Refmem_Alloc_event(&EventData.refmem);
   if(event == NULL)
   {
      return NULL;
   }
   event->type = eET_ShipAttack;
   event->shipAttack.attacker = attacker;
   event->shipAttack.target   = target;
   event->shipAttack.dammage  = dammage;
   Ship_Take(attacker);
   Ship_Take(target);
   return event;   
}

union event * Event_CreateShipDestroyed(struct ship * ship)
{
   union event * event = Refmem_Alloc_event(&EventData.refmem);
   if(event == NULL)
   {
      return NULL;
   }
   event->type = eET_ShipDestroyed;
   event->shipDestroyed.ship = ship;
   Ship_Take(ship);
   return event;   
}

void Event_Take(union event * event)
{
   Refmem_Take_event(&EventData.refmem, event);
}

bool Event_Release(union event * event)
{
   bool deleted = Refmem_Release_event(&EventData.refmem, event);
   if(deleted)
   {
      switch(event->type)
      {
      case eET_None:
         // Do nothing on prupose
         break;
      case eET_ShipAttack:
         Ship_Release(event->shipAttack.attacker);
         Ship_Release(event->shipAttack.target);
         break;
      case eET_ShipDestroyed:
         Ship_Release(event->shipDestroyed.ship);
         break;
      }
   }
   return deleted;
}

