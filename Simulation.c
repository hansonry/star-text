#include "Simulation.h"
#include "List.h"
#include "Refmem.h"



void Simulation_Init(void)
{
   Event_Init();
   PlayerList_Init();
   ShipList_Init();
   
}

static inline
void Simulation_SendEvent(union event * event)
{
   if(event != NULL)
   {
      size_t i, count;
      struct ship ** ships = ShipList_Get(&count);
      for(i = 0; i < count; i++)
      {
         struct ship * ship = ships[i];
         PlayerList_EventQueue(ship->owner, event);
      }
      /*
      switch(event->type)
      {
      case eET_None:
         // Do nothing on prupose
         break;
      case eET_ShipAttack:
         PlayerList_EventQueue(event->shipAttack.attacker->owner, event);
         PlayerList_EventQueue(event->shipAttack.target->owner, event);
         break;
      case eET_ShipDestroyed:
         PlayerList_EventQueue(event->shipDestroyed.ship->owner, event);
         break;
      }
      */
      Event_Release(event);
   }
}

void Simulation_Step(void)
{
   size_t i, count;
   struct ship ** ships;
   
   ships = ShipList_Get(&count);
   for(i = 0; i < count; i++)
   {
      struct ship * ship = ships[i];
      struct position next;
      if(Bresenham3D_Next(&ship->movement.b3d, &next))
      {
         Position_Copy(&ship->position, &next);
      }
      // Weapons

      // Check to see if the target is dead
      if(ship->weapons.target != NULL &&
         ship->weapons.target->health == 0)
      {
         Ship_Release(ship->weapons.target);
         ship->weapons.target = NULL;
      }
      // Check to see if  we can fire
      if(ship->weapons.target != NULL &&
         ship->weapons.fireAtWill && 
         ship->weapons.reloadTimer == 0)
      {
         // Apparently we have perfect aim
         const unsigned int dammage = 45;
         struct ship * targetShip = ship->weapons.target;
         union event * event = Event_CreateShipAttack(ship, targetShip, dammage);
         
         if(targetShip->health > dammage)
         {
            targetShip->health -= dammage;
         }
         else
         {
            targetShip->health = 0;
         }
         
         Simulation_SendEvent(event);
 
         // Reset reload timer
         ship->weapons.reloadTimer += 3;
      }
      if(ship->weapons.reloadTimer > 0)
      {
         ship->weapons.reloadTimer --;
      }
   }
   
   // Message Dead Ships
   for(i = 0; i < count; i++)
   {
      struct ship * ship = ships[i];
      if(ship->health == 0)
      {
         union event * event = Event_CreateShipDestroyed(ship);
         Simulation_SendEvent(event);
      }
   }

   // Remove Dead ships
   ShipList_RemoveDead();
}

static inline
void Simulation_CommandMoveShip(struct ship * ship, 
                                const struct commandMove * command)
{
   Position_Copy(&ship->movement.destination, &command->destination);
   Bresenham3D_Init(&ship->movement.b3d, &ship->position, 
                                         &ship->movement.destination);
}

static inline
void Simulation_CommandTargetShip(struct ship * ship,
                                  const struct commandTarget * target)
{
   if(ship->weapons.target != NULL)
   {
      Ship_Release(ship->weapons.target);
   }
   ship->weapons.target = target->target;
   
   if(ship->weapons.target != NULL)
   {
      Ship_Take(ship->weapons.target);
   }
}

static inline
void Simulation_CommandWeaponsShip(struct ship * ship,
                                   const struct commandWeapons * weapons)
{
   ship->weapons.fireAtWill = weapons->fireAtWill;
}

void Simulation_CommandShip(struct ship * ship, const union command * command)
{
   switch(command->type)
   {
   case eCT_None:
      // Do nothing on purpose
      break;
   case eCT_Move:
      Simulation_CommandMoveShip(ship, &command->move);
      break;
   case eCT_Target:
      Simulation_CommandTargetShip(ship, &command->target);
      break;
   case eCT_Weapons:
      Simulation_CommandWeaponsShip(ship, &command->weapons);
      break;
   }
}



