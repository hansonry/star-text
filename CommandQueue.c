#include "CommandQueue.h"
#include "Command.h"
#include <string.h>

void CommandQueue_Init(struct commandQueue * queue, size_t sizeInElements, void * data)
{
   queue->size  = sizeInElements;
   queue->base  = data;
   queue->count = 0;
   queue->head  = 0;
   queue->tail  = 0;
}

void CommandQueue_Clear(struct commandQueue * queue)
{
   queue->count = 0;
   queue->head  = 0;
   queue->tail  = 0;
}

static inline 
size_t indexIncrement(struct commandQueue * queue, size_t value)
{
   return (value + 1) % queue->size;
}

bool CommandQueue_Enqueue(struct commandQueue * queue, const union command * command)
{
   size_t index = 0;
   if(queue->count >= queue->size)
   {
      return false;
   }
   
   index = queue->tail;
   queue->tail = indexIncrement(queue, queue->tail);
   queue->count ++;
   
   memcpy(&queue->base[index], command, sizeof(union command));
   
   return true;
}

bool CommandQueue_Deque(struct commandQueue * queue, union command * command)
{
   size_t index = 0;
   if(queue->count <= 0)
   {
      return false;
   }
   index = queue->head;
   queue->head = indexIncrement(queue, queue->head);
   queue->count ++;
   
   memcpy(command, &queue->base[index], sizeof(union command));
   
   return true;
}

