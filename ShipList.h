#ifndef __SHIPLIST_H__
#define __SHIPLIST_H__
#include <stdbool.h>
#include "Position.h"
#include "Bresenham3D.h"
#include "List.h"

struct player;
struct ship;

struct shipMovement
{
   struct position destination;
   struct bresenham3d b3d;
};

struct shipWeapons
{
   struct ship * target;
   bool fireAtWill;
   unsigned int reloadTimer;
};

struct ship
{
   struct position position;
   unsigned int health;
   struct shipMovement movement;
   struct shipWeapons weapons;
   struct player * owner;
};

LIST_CREATE_TYPE(ship, struct ship *)

void ShipList_Init(void);
struct ship ** ShipList_Get(size_t * count);

bool Ship_Release(struct ship * ship);
void Ship_Take(struct ship * ship);

void ShipList_RemoveDead(void);

struct ship * ShipList_AddShip(struct player * owner, int x, int y, int z);

void Ship_Kill(struct ship * ship);

#endif // __SHIPLIST_H__

