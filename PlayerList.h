#ifndef __PLAYER_H__
#define __PLAYER_H__
#include "List.h"
#include "Event.h"

#define PLAYER_EVENT_QUEUE_SIZE 64

struct player
{
   int id;
   union event * eventData[PLAYER_EVENT_QUEUE_SIZE];
   struct queue_event eventQueue;
};

LIST_CREATE_TYPE(player, struct player *)

void PlayerList_Init(void);
void PlayerList_Take(struct player * player);
bool PlayerList_Release(struct player * player);
struct player * PlayerList_New(void);
void PlayerList_EventQueue(struct player * player, union event * event);
union event * PlayerList_EventDequeue(struct player * player);

#endif // __PLAYER_H__
