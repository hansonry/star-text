#ifndef __QUEUE_H__
#define __QUEUE_H__
#include <stddef.h>
#include <stdbool.h>

struct queue
{
   void * base;
   size_t elementSize;
   size_t size;
   size_t count;
   size_t head;
   size_t tail;
};

void Queue_Init(struct  queue * queue, size_t elementSize, 
                size_t size, void * memory);
void * Queue_EnqueuePtr(struct queue * queue);
size_t Queue_EnqueueCopy(struct queue * queue, const void * value, size_t count);
void * Queue_PeekPtr(const struct queue * queue);
bool Queue_PeekCopy(const struct queue * queue, void * outValue);
bool Queue_Dequeue(struct queue * queue);
bool Queue_DequeueCopy(struct queue * queue, void * outValue);
void Queue_Clear(struct queue * queue);

#define QUEUE_CREATE_TYPE(name, type)                                          \
struct queue_ ## name                                                          \
{                                                                              \
   struct queue queue;                                                         \
};                                                                             \
                                                                               \
static inline                                                                  \
void Queue_Init_ ## name (struct queue_ ## name * queue, size_t size,          \
                          type * memory)                                       \
{                                                                              \
   Queue_Init(&queue->queue, sizeof(type), size, memory);                      \
}                                                                              \
                                                                               \
static inline                                                                  \
type * Queue_EnqueuePtr_ ## name (struct queue_ ## name * queue)               \
{                                                                              \
   return Queue_EnqueuePtr(&queue->queue);                                     \
}                                                                              \
                                                                               \
static inline                                                                  \
size_t Queue_EnqueueCopy_ ## name (struct queue_ ## name * queue,              \
                                   const type * value, size_t count)           \
{                                                                              \
   return Queue_EnqueueCopy(&queue->queue, value, count);                      \
}                                                                              \
                                                                               \
static inline                                                                  \
bool Queue_Enqueue_ ## name (struct queue_ ## name * queue,                    \
                             type  value)                                      \
{                                                                              \
   return Queue_EnqueueCopy(&queue->queue, &value, 1) == 1;                    \
}                                                                              \
                                                                               \
static inline                                                                  \
type * Queue_PeekPtr_ ## name (struct queue_ ## name * queue)                  \
{                                                                              \
   return Queue_PeekPtr(&queue->queue);                                        \
}                                                                              \
                                                                               \
static inline                                                                  \
bool Queue_PeekCopy_ ## name (const struct queue_ ## name * queue,             \
                              type * outValue)                                 \
{                                                                              \
   return Queue_PeekCopy(&queue->queue, outValue);                             \
}                                                                              \
                                                                               \
static inline                                                                  \
type Queue_Peek_ ## name (const struct queue_ ## name * queue)                 \
{                                                                              \
   type t;                                                                     \
   (void)Queue_PeekCopy(&queue->queue, &t);                                    \
   return t;                                                                   \
}                                                                              \
                                                                               \
static inline                                                                  \
bool Queue_Dequeue_ ## name (struct queue_ ## name * queue)                    \
{                                                                              \
   return Queue_Dequeue(&queue->queue);                                        \
}                                                                              \
                                                                               \
static inline                                                                  \
bool Queue_DequeueCopy_ ## name (struct queue_ ## name * queue,                \
                                 type * outValue)                              \
{                                                                              \
   return Queue_DequeueCopy(&queue->queue, outValue);                          \
}                                                                              \
                                                                               \
static inline                                                                  \
type Queue_DequeueReturn_ ## name (struct queue_ ## name * queue)              \
{                                                                              \
   type t;                                                                     \
   (void)Queue_DequeueCopy(&queue->queue, &t);                                 \
   return t;                                                                   \
}                                                                              \
                                                                               \
static inline                                                                  \
void Queue_Clear_ ## name (struct queue_ ## name * queue)                      \
{                                                                              \
   Queue_Clear(&queue->queue);                                                 \
}                                                                              \
                                                                               \
static inline                                                                  \
size_t Queue_GetUsedCount_ ## name (struct queue_ ## name * queue)             \
{                                                                              \
   return queue->queue.count;                                                  \
}                                                                              \
                                                                               \
static inline                                                                  \
size_t Queue_GetFreeCount_ ## name (struct queue_ ## name * queue)             \
{                                                                              \
   return queue->queue.size - queue->queue.count;                              \
}                                                                              \
                                                                               \
static inline                                                                  \
bool Queue_NotEmpty_ ## name (struct queue_ ## name * queue)                   \
{                                                                              \
   return queue->queue.count > 0;                                              \
}


#endif // __QUEUE_H__

