#ifndef __POSITION_H__
#define __POSITION_H__
#include <stdbool.h>
#include <math.h>

enum positionComponent
{
   ePC_X,
   ePC_Y,
   ePC_Z
};

struct position
{
   int x;
   int y;
   int z;
};

static inline
struct position * Position_Set(struct position * pos, int x, int y, int z)
{
   pos->x = x;
   pos->y = y;
   pos->z = z;
   return pos;
}

static inline
struct position * Position_Copy(struct position * dest, 
                                const struct position * src)
{
   dest->x = src->x;
   dest->y = src->y;
   dest->z = src->z;
   return dest;
}

static inline 
struct position * Position_SetComponent(struct position * pos, 
                                        enum positionComponent comp, 
                                        int value)
{
   switch(comp)
   {
   case ePC_X:
      pos->x = value;
      break;
   case ePC_Y:
      pos->y = value;
      break;
   case ePC_Z:
      pos->z = value;
      break;
   }
   return pos;
}

static inline
int Position_GetComponent(const struct position * pos,
                          enum positionComponent comp)
{
   int value = 0;
   switch(comp)
   {
   case ePC_X:
      value = pos->x;
      break;
   case ePC_Y:
      value = pos->y;
      break;
   case ePC_Z:
      value = pos->z;
      break;
   }
   return value;
}

static inline
enum positionComponent Position_GetLargestComponent(const struct position * pos)
{
   enum positionComponent comp = ePC_X;
   int value = pos->x;
   if(value < pos->y)
   {
      value = pos->y;
      comp = ePC_Y;
   }
   if(value < pos->z)
   {
      value = pos->z;
      comp = ePC_Z;
   }
   return comp;
}

static inline
struct position * Position_Map(struct position * pos, 
                               const enum positionComponent comps[3])
{
   struct position backup;
   Position_Copy(&backup, pos);
   Position_SetComponent(pos, comps[0], backup.x);
   Position_SetComponent(pos, comps[1], backup.y);
   Position_SetComponent(pos, comps[2], backup.z);
   return pos;
}

static inline
struct position * Position_Unmap(struct position * pos, 
                                 const enum positionComponent comps[3])
{
   struct position backup;
   Position_Copy(&backup, pos);
   pos->x = Position_GetComponent(&backup, comps[0]);
   pos->y = Position_GetComponent(&backup, comps[1]);
   pos->z = Position_GetComponent(&backup, comps[2]);
   return pos;
}


static inline
bool Position_IsEqual(const struct position * a, const struct position * b)
{
   return a->x == b->x &&
          a->y == b->y &&
          a->z == b->z;
}

static inline
struct position * Position_Subtract(struct position * a, const struct position * b)
{
   a->x -= b->x;
   a->y -= b->y;
   a->z -= b->z;
   return a;
}


static inline
int Position_Length2(const struct position * pos)
{
   return pos->x * pos->x + pos->y * pos->y + pos->z * pos->z;
}


static inline
double Position_Length(const struct position * pos)
{
   return sqrt(Position_Length2(pos));
}



#endif // __POSITION_H__
