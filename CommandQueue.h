#ifndef __COMMANDQUEUE_H__
#define __COMMANDQUEUE_H__
#include <stddef.h>
#include <stdbool.h>

union command;

struct commandQueue
{
   union command * base;
   size_t head;
   size_t tail;
   size_t count;
   size_t size;
};

void CommandQueue_Init(struct commandQueue * queue, size_t sizeInElements, void * data);
void CommandQueue_Clear(struct commandQueue * queue);
bool CommandQueue_Enqueue(struct commandQueue * queue, const union command * command);
bool CommandQueue_Deque(struct commandQueue * queue, union command * command);

static inline
size_t CommandQueue_GetCount(const struct commandQueue * queue)
{
   return queue->count;
}

#endif // __COMMANDQUEUE_H__
